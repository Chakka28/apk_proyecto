﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Apk_Proyecto.Entity;
using System.Collections.ObjectModel;

namespace Apk_Proyecto
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Crud : ContentPage
    {
        public ObservableCollection<Users> datosInfo { get; private set; }//Lista de datos
        private string username;// El username del usuario logeado

        public Crud(ObservableCollection<Users> datos, string usernameU)
        {
            InitializeComponent();
            datosInfo = new ObservableCollection<Users>();
            datosInfo = datos;
            username = usernameU;
            BindingContext = this;
        }

        //Redirecciona al formulario de crear usuario
        public void SaveUser(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new formulario(datosInfo, username));
        }

        //Redirecciona al formulario de  editar usuario
        public void EditUser(object sender, EventArgs e)
        { 
            Users contexto = (Users)list.SelectedItem;
            if (contexto == null)
            {
                DisplayAlert("Alert", "Select an account", "OK");
            }
            else
            {
                Navigation.PushModalAsync(new formulario(datosInfo, contexto, username));
            }
        }

        //Elimina el usuario
        public async void Delete(object sender, EventArgs e)
        {
            Users contexto = (Users)list.SelectedItem;

            if(contexto == null)
            {
                await DisplayAlert("Alert", "Select an account", "OK");
            }
            if (contexto.user == username)
            {
               bool result =  await DisplayAlert("Alert", "This user is logged in", "Ok", "Cancel");
                if (result)
                {
                    datosInfo.Remove(contexto);
                    await Navigation.PushModalAsync(new MainPage(datosInfo));
                }
            }
            else
            {
                datosInfo.Remove(contexto);
            }
            
        }

        //Cierra sesion 
        public async void sign_off(object sender, EventArgs e)
        {
            bool result = await DisplayAlert("Alert", "Are you sure you want to log out?", "Ok", "Cancel");
            if (result)
            {
                await Navigation.PushModalAsync(new MainPage(datosInfo));
            }
        }

    }
}