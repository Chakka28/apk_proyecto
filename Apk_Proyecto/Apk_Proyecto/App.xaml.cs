﻿using Apk_Proyecto.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apk_Proyecto
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            ObservableCollection<Users> datosInfo = new ObservableCollection<Users>();
            datosInfo.Add(new Entity.Users { user = "User1", password = "123" , email="user1@gmail.com"});
            datosInfo.Add(new Entity.Users { user = "User3", password = "123", email = "user3@gmail.com" });
            MainPage = new MainPage(datosInfo);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
