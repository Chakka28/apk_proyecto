﻿using Apk_Proyecto.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Apk_Proyecto
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Users> datosInfo { get; private set; }//Lista de usuarios para logear

        public MainPage(ObservableCollection<Users> datos)
        {
            InitializeComponent();
            datosInfo = datos;
        }

        //Metodo principal donde se verifica el usuario
        public  void AuthUser(object sender, EventArgs e)
        {
            var m = user.Text;
            var p = pass.Text;
            if(m != "" && p != "")
            {
               if (through_list())
                 {
                     Navigation.PushModalAsync(new Crud(datosInfo, m));
                 }
                 else
                 {
                    DisplayAlert("Alert", "Username or password is incorrect", "OK");
                 }
            }
            else
            {
                DisplayAlert("Alert", "there is null data", "OK");
            }
        }

        // Recorre la lista y comprueba que el usuario existe
        public bool through_list()
        {
            var m = user.Text;
            var p = pass.Text;
            foreach (Users item in datosInfo)
            {
                if (m == item.user && p == item.password)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
