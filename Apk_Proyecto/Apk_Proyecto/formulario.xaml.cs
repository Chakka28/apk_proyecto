﻿using Apk_Proyecto.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Apk_Proyecto
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class formulario : ContentPage
    {
        public ObservableCollection<Users> datosInfo { get; private set; }//Lista de datos
        string funcion;//Crear para nuevo usuario/Modificar cuando se  va a editar
        Users userInfo;// Usuario que se quiere editar
        string userlogin;//Usuario que esta logeado a la app

        //Constructor en caso de crear nuevo usuario
        public formulario(ObservableCollection<Users> datos, string userLogin)
        {
            datosInfo = new ObservableCollection<Users>();
            datosInfo = datos;
            userlogin = userLogin;
            BindingContext = this;
            funcion = "crear";
            InitializeComponent();
        }

        //Constructor en caso de modificar usuario
        public formulario(ObservableCollection<Users> datos, Users userInfoR, string userLogin)
        {
            datosInfo = new ObservableCollection<Users>();
            datosInfo = datos;
            userInfo = userInfoR;
            userlogin = userLogin;
            BindingContext = this;
            funcion = "Modificar";
            InitializeComponent();
            fill_data();
        }

        //Metodo que rellena  los datos en caso de editar usuario
        public void fill_data()
        {
            user.Text = userInfo.user;
            email.Text = userInfo.email;
            pass.Text = userInfo.password;
        }

        //Metodo princpal donde se edita o se crea un nuevo usuario
        public void save(object sender, EventArgs e)
        {
            var u = user.Text;
            var p = pass.Text;
            var pa = passA.Text;
            var m = email.Text;
            if(u != "" & p != "" & m != "" & pa != "")
            {
                if(p == pa)
                {
                    if(funcion == "crear")
                    {
                        if (verify(u))
                        {

                            datosInfo.Add(new Users { user = u, email = m, password = p });
                            DisplayAlert("User", "new user added", "OK");
                            Navigation.PushModalAsync(new Crud(datosInfo, userlogin));
                        }
                        else
                        {
                            DisplayAlert("Username", "This username already exists", "OK");
                        }
                    }else
                    {
                        datosInfo.Remove(userInfo);
                        datosInfo.Add(new Users { user = u, email = m, password = p });
                        username(u);
                        DisplayAlert("User", "Modified user", "OK");
                        Navigation.PushModalAsync(new Crud(datosInfo, userlogin));
                    }
                }
                else
                {
                    DisplayAlert("Password", "Passwords do not match", "OK");
                }
            }
            else
            {
                DisplayAlert("Null", "you wrote empty data", "OK");
            }
                
        }

        //Verifica que no se agregue un usuario que ya existe
        public bool verify(string username)
        {
            foreach (Users item in datosInfo)
            {
                if (item.user == username)
                {
                    return false;
                }
            }
            return true;
        }

        //Verifica que el usuario que se esta editando sea el que se esta logeando
        public void username(string user)
        {
            if(userlogin == userInfo.user)
            {
                userlogin = user;
            }
        }
    }
}